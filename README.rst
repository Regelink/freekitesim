Free Kite Power System Simulator
--------------------------------

This project aims at the development of a free kite-power system simulator
for educational purposes.

Intended use:

a) Provide a learning environment for students who are studying airborne
   wind energy.
b) Training of kite pilots and winch operators. In this case kite and winch
   are operated with joysticks.
c) Development and test of automatic control software for kite-power systems.

This software is published under the LGPL license.

If you have any suggestions and fixes to improve this software please write
a posting on https://groups.google.com/forum/#!forum/free-kitesim .

Installation
------------

The manual installation is explained in:
`INSTALL_01.txt <http://bitbucket.org/ufechner/freekitesim/src/master/INSTALL_01.txt>`_

For fast, dynamic simulations the installation of numba (a Python compiler) is
required, which is explained in:
`INSTALL_02.txt <http://bitbucket.org/ufechner/freekitesim/src/master/INSTALL_02.txt>`_

How to use
----------

The Software can be used in two different modes at this point. 

- The log-file viewer allows to replay test-flights by reading the recorded data
  and showing the movement in a 3D animation 
- The dynamic simulator offers the user the possiblity to tweak parameters and
  test control software with a four-point kite model

The two different programs can be run with the executable scripts kitesim.sh and
kiteplayer.sh. Obviously the former runs the simulation and the other the
log-file viewer. 

How to use the log-file viewer
------------------------------

a) Run the 3D viewer and the GUI-based log file player::

    cd ~/00PythonSoftware/FreeKiteSim
    ./kiteplayer.sh

b) Rearange the windows so that they do not overlap. Select a log file
   in the combobox on the left and press play.

Please look also at the `FreeKiteSim <freekitesim/src/master/doc/FreeKiteSim.rst>`_ manual.
A .pdf version of the documentation can be created with the following commands::

    cd doc
    ./make.sh

How to use the dynamic simulator
--------------------------------

The dynamic simulation is done with the RADAU solver from the ASSIMULO solver suite.
One simple example can be found in the example folder.

The following files belong to the dynamic simulator:

- Timer.py        (timer class, used for benchmarking)
- linalg_3d.py    (fast 3D linear algebra routines)
- KCU_Sim.py      (simulator for the kite control unit)
- KPS4P.py        (Python version of the 4-point kite model)
- WinchModel.py   (model of the 20 kW winch of TU Delft)
- Kite_4_point.py (script to construct the initial positions of the kite particles)
- RTSim.py        (simulator core, based on Assimulo and using the RADAU solver)
- KiteViewer3.py  (extended version of the 3D kite viewer)

To run the simulation:

1. connect a joystick, e.g. Logitech Gamepad F310 or Logitech Extreme 3D Pro (prefered)
2. open a terminal and enter::

    cd ~/00PythonSoftware/FreeKiteSim/
    ./kitesim.sh

3. wait until the message "Simulation started!" is printed in the console

Now you can steer by pushing the joystick to the right or to the left, and operate the
winch by turning the joystick (if you use the Extreme 3D joystick).

The simulation speed is not yet realtime, but 6-7 times slower than realtime on a core i7, 3.4 GHz.
To improve the simulation speed the first step is to recode the functions calc_cl and calc_cd
in KPS4P.py using lookup-tables and linear interpolation.

Delft University of Technology, section wind energy, The Netherlands.