QT       += core
QT       += network

QT       -= gui

TARGET = Utils
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

HEADERS += \
    utils.h

SOURCES += \
    utils.cpp

OTHER_FILES += \
    Readme.txt

SOURCES += main.cpp
