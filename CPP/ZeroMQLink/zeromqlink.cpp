/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "zeromqlink.h"
#ifndef OROCOS
#include "QsLog.h"
#else
#include <iostream>
#endif
#include <QDebug>
#include <QSemaphore>
#include <exception>
#include "utils.h"
#include "assert.h"

using namespace std;

QSemaphore sleepSemaphore(1);

ZeroMQLink::ZeroMQLink(QObject *parent) :
    AbstractLink(parent)
{
    // create the ZeroMQ context, using 1 IO thread
    m_context = new zmq::context_t(1);
    // create a publisher socket
    m_publisher = new zmq::socket_t(*m_context, ZMQ_PUB);
    // create new thread for receiving messages
    m_receiveThread = new ReceiveThread(m_context);
    connect(m_receiveThread, SIGNAL(messageReceived(zmq::message_t *)), this, SLOT(onMessageReceived(zmq::message_t *)));
}

ZeroMQLink::~ZeroMQLink()
{    
#ifndef OROCOS
    QLOG_INFO() << "destroying ZeroMQLink.";
#endif
    m_publisher->close();
#ifndef OROCOS
    QLOG_INFO() << "destroying ZeroMQLink..";
#endif
    m_receiveThread->finish();
#ifndef OROCOS
    QLOG_INFO() << "destroying ZeroMQLink...";
#endif
    delete m_publisher;
#ifndef OROCOS
    QLOG_INFO() << "destroying ZeroMQLink....";
#endif
    Utils::usleep(50000);
#ifndef OROCOS
    QLOG_INFO() << "destroying ZeroMQLink.....";
#endif
    if (m_receiveThread->isFinished()) {
        delete m_receiveThread;
    }
#ifndef OROCOS
    QLOG_INFO() << "destroying ZeroMQLink......";
#endif
}

void ZeroMQLink::sendMessage(QByteArray message, int msg_type)
{
    assert(message.size() < MAX_MSG_SIZE);
    // prepend the message with a byte, that encodes the message type
    message.prepend((quint8)msg_type);
    zmq::message_t z_message(message.length());
    memcpy ((void *) z_message.data(), message.data(), message.length());
    m_publisher->send(z_message);
}

int ZeroMQLink::bind(QString connection)
{
    // close the old publisher socket
    m_publisher->close();
    Utils::usleep(10000);
    // create a publisher socket
    m_publisher = new zmq::socket_t(*m_context, ZMQ_PUB);
    int result = 0;
    try {
        m_publisher->bind(connection.toUtf8().constData());
    } catch (exception& e) {
#ifndef OROCOS
        QLOG_ERROR() << "Couldn't bind to "<< connection << "!";
#else
        std::cout << "Couldn't bind to "<< connection.toStdString() << "!";
#endif
        result = 1;
    }
    return (result);
}

int ZeroMQLink::connect_link(QString connection)
{
    int result = m_receiveThread->connect_link(connection);
    return (result);
}

void ZeroMQLink::subscribe(int msg_type)
{
    m_receiveThread->subscribe(msg_type);
}

void ZeroMQLink::start()
{
    m_receiveThread->start();
    // sleepSemaphore.release();
    // m_receiveThread->setPriority(QThread::TimeCriticalPriority);
}

QByteArray ZeroMQLink::getLastMessage(int msg_type)
{
    QByteArray nothing;
    if (msg_type == m_lastMsgType) {
        sleepSemaphore.release();
        return(m_qmessage);
    } else return nothing;
}

void ZeroMQLink::onTimer()
{
}

void ZeroMQLink::onMessageReceived(zmq::message_t* message)
{
#ifndef OROCOS
    QLOG_TRACE() << "onMessageReceived";
#endif
    // store message in a QByteArray while removing the message type
    m_qmessage = QByteArray((const char *)message->data() + 1, message->size() - 1);
    const char* type = (const char *)message->data();
    m_lastMsgType = (int) *type;
    messageReceived(m_lastMsgType);
}

ReceiveThread::ReceiveThread(zmq::context_t *context)
{
    // create the ZeroMQ context, using 1 IO thread
    // m_context = new zmq::context_t(1);
    m_context = context;
    // create a subscriber socket
    m_subscriber = new zmq::socket_t(*m_context, ZMQ_SUB);
    // initialize message buffer
    m_message = new zmq::message_t(MAX_MSG_SIZE);
    //
    m_finish = false;
}

ReceiveThread::~ReceiveThread()
{
#ifndef OROCOS
    QLOG_INFO() << "destroying ReceiveThread.";
#endif
    delete m_message;
#ifndef OROCOS
    QLOG_INFO() << "destroying ReceiveThread..";
#endif
    m_subscriber->close();
#ifndef OROCOS
    QLOG_INFO() << "destroying ReceiveThread...";
#endif
    delete m_subscriber;
    delete m_context;
}

void ReceiveThread::run()
{
    //
    exec();
}

int ReceiveThread::connect_link(QString connection)
{
    int result = 0;
    try
    {
        m_subscriber->connect(connection.toStdString().data());
    }
    catch (exception& e)
    {
#ifndef OROCOS
        QLOG_ERROR() << "Couldn't connect to " << connection << "!";
#endif
        result = 2;
    }
    // QLOG_INFO() << "Result of connect_link in ReceiveThread: " << result;
    return (result);
}

void ReceiveThread::subscribe(int msg_type)
{
    QByteArray filter;
    filter.prepend((quint8)msg_type);
    QLOG_INFO() << "Subscribing to: " << filter.toHex();
    m_subscriber->setsockopt(ZMQ_SUBSCRIBE, filter.data(), 1);
}

int ReceiveThread::exec()
{
    while(!m_finish)
    {
        sleepSemaphore.acquire();        
        QLOG_TRACE()  << "Waiting for message: " << m_message;
        // read data, blocking
        bool result = m_subscriber->recv (m_message);
        if (result) {
#ifndef OROCOS
            QLOG_TRACE() << "Message received!";
#endif
            QLOG_TRACE()  << "Message received!";
            emit messageReceived(m_message);
        } else {
#ifndef OROCOS
            QLOG_INFO() << "None...";
#endif
        }
        // process received message
    }
#ifndef OROCOS
    QLOG_INFO() << "Finished receive thread!";
#endif
    return (0);
}

void ReceiveThread::finish()
{
    m_finish = true;
    sleepSemaphore.release();
}
