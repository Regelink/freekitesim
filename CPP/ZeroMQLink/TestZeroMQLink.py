#!/usr/bin/env python
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
""" This module provides the class Publisher for publishing ParticleSystem, EstimatedSystemState
and Event messages on port 5575. """
import zmq
import time as ti
import sys
sys.path.append('/home/ufechner/00PythonSoftware/KiteSim')
import asset.system_pb2 as system
import asset.pod_pb2 as pod
from math import pi
import numpy as np

SEGMENTS = 6

# pylint: disable=E1101
class Publisher(object):
    """Publisher for ZeroMQ messages. Is publishing on port ParticleSystem messages
       and EstimatedSystemState messages on port 5575. """
    def __init__(self):
        self.context = zmq.Context()
        self.sock = self.context.socket(zmq.PUB)
        self.sock.bind('tcp://*:5575')
        ti.sleep(0.5) # wait until the connection is established before sending any messages
        self.sys = system.ParticleSystem()
        self.state = system.EstimatedSystemState()
        self.pilot = system.PilotCtrlState()
        self.pod_state = pod.PodState()
        self.time = 0.0
        self.counter = 0
        self.event_counter = 0
        self.pilot_counter = 0
        self.pod_state_counter = 0
    def publishParticleSystem(self, pos, v_reel_out, v_app, l_tether, force, azimuth, elevation, \
                                    vel_kite, prediction = [], segments=SEGMENTS):
        """
        Publish the ParticleSystem message (for the KiteViewer)
        Parameters:
        pos: vector of the positions of the particles """
        self.sys.Clear()
        self.sys.time = self.time
        self.time += 0.05 # one new message every 50 ms
        self.sys.counter = self.counter
        self.counter += 1
        #for particle in range(segments+1):
        for particle in range(pos.shape[0]):
            position = self.sys.positions.add()
            position.pos_east  = pos[particle][0]
            position.pos_north = pos[particle][1]
            position.height    = pos[particle][2]
        for particle in range(len(prediction)):
            position = self.sys.pos_predicted.add()
            position.pos_east  = prediction[particle][0]
            position.pos_north = prediction[particle][1]
            position.height    = prediction[particle][2]
        velocity = self.sys.velocities.add()
        velocity.v_east  = vel_kite[0]
        velocity.v_north = vel_kite[1]
        velocity.v_up    = vel_kite[2]
        self.sys.v_reel_out = v_reel_out
        self.sys.l_tether   = l_tether
        self.sys.force      = force
        self.sys.azimuth, self.sys.elevation = azimuth, elevation
        self.sys.v_app_x, self.sys.v_app_y, self.sys.v_app_z = v_app[0], v_app[1], v_app[2]
        binstr = chr(21) + self.sys.SerializeToString()
        self.sock.send(binstr)
        # print "vel_kite: ", vel_kite

    def publishEstSysState(self, pos, velocity_w, v_app_norm, avg_winddirection_ground, elevation, azimuth, \
                                 length, heading, course, heading_d, course_d):
        """ Publish the estimated system state (for FrontView etc.) """
        self.state.Clear()
        self.state.header.time_rel = self.time
        self.state.pos_east = pos[0]
        self.state.pos_north = pos[1]
        self.state.height = pos[2]
        self.state.velocity_w.vx = velocity_w[0]
        self.state.velocity_w.vy = velocity_w[1]
        self.state.velocity_w.vz = velocity_w[2]
        self.state.apparent_windvelocity_kite = v_app_norm
        self.state.avg_winddirection_ground = avg_winddirection_ground
        self.state.elevation = elevation
        self.state.azimuth = azimuth
        self.state.tether_length_reelout = length
        self.state.heading = heading
        self.state.course = course
        self.state.heading_d.hx, self.state.heading_d.hy = heading_d
        self.state.course_d.cx, self.state.course_d.cy = course_d
        binstr = chr(14) + self.state.SerializeToString()
        self.sock.send(binstr)

    def publishPodState(self, orient):
        """ Publish the pod state (mainly for 3LAP). Currently only the orientation. """
        self.pod_state.Clear()
        self.pod_state.time = self.time
        self.pod_state.time_sent = self.time
        self.pod_state.counter = self.pod_state_counter
        self.pod_state_counter += 1
        self.pod_state.kite_state.orientation.phi = orient[0] * 180.0 / pi
        self.pod_state.kite_state.orientation.theta = orient[1] * 180.0 / pi
        self.pod_state.kite_state.orientation.psi = orient[2] * 180.0 / pi
        binstr = chr(8) + self.pod_state.SerializeToString()
        self.sock.send(binstr)

    def publishPilotCtrlState(self, depower, steering):
        self.pilot.Clear()
        self.pilot.counter = self.pilot_counter
        self.pilot_counter += 1
        self.pilot.time = ti.time()
        self.pilot.time_sent = ti.time()
        self.pilot.rel_depower = depower * 100.0
        self.pilot.rel_steering = steering * 100.0
        binstr = chr(13) + self.pilot.SerializeToString()
        self.sock.send(binstr)

    def getEvent(self, event):
        m_event = system.EventMsg()
        m_event.counter = self.event_counter
        m_event.time = ti.time()
        m_event.event = event
        if event==system.FAST_CLK:
            m_event.event_source = system.CLOCK
        else:
            m_event.event_source = system.SYSTEM_OPERATOR
        self.event_counter += 1
        return m_event

    def sendMessage(self, message, msg_type):
        binstr = chr(msg_type) + message.SerializeToString()
        #print binstr.encode('hex_codec')
        self.sock.send(binstr)

    def sendEvent(self, event):
        self.sendMessage(self.getEvent(event), system.mtEventMsg)

    def close(self):
        self.sock.close()

if __name__ == "__main__":
    print "\nPublishing 10 EstimatedSystemState messages on port 5575..."
    pub = Publisher()
    pos = np.array((1., 2., 3.))
    velocity_w = np.array((8., 0.1, 0.))
    v_app_norm = 20.0
    avg_winddirection_ground = 0.2
    elevation = 0.5
    azimuth = 0.4
    length = 150.0
    heading = 20.0
    course = 22.0
    heading_d = (3.,4.)
    course_d = (4., 5.)
    for i in range(10):
        if False:
            pub.publishEstSysState(pos, velocity_w, v_app_norm, avg_winddirection_ground, elevation, azimuth, \
                               length, heading, course, heading_d, course_d)
        if True:
            pub.sendEvent(system.FAST_CLK)
        ti.sleep(0.05)


