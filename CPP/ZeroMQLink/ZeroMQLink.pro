#-------------------------------------------------
#
# Project created by QtCreator 2012-01-14T13:41:18
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = ZeroMQLink
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# BEGIN section protobuf (only for testing)
PROTOS = ../../00AssetLib/ProtoBuf/asset.pod.proto \
         ../../00AssetLib/ProtoBuf/asset.winch.proto \
         ../../00AssetLib/ProtoBuf/asset.system.proto

include(../ProtoBuf/protobuf2.pri)

LIBS += /usr/lib/libprotobuf.so
# END section protobuf

# this is specific for projects, that use zeromq
LIBS    += /usr/lib/libzmq.so

SOURCES += main.cpp \
    zeromqlink.cpp \
    abstractlink.cpp \
    testzeromqlink.cpp

OTHER_FILES += \
    Readme.txt \
    TestZeroMQLink.py

HEADERS += \
    zeromqlink.h \
    abstractlink.h \
    testzeromqlink.h

include(../../00AssetLib/QsLog/QsLog.pri)
include(../../00AssetLib/Utils/utils.pri)
