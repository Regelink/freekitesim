// option optimize_for = LITE_RUNTIME;
package asset.pod;        // define all messages, that are used for the communication with the pod
                          // and for the log files, that are recorded within the pod

// state of the ASTI board, not used during the first latency test
enum PodCtrlState {
  REMOTE_CONTROL     = 0; // unidirectional RC control active
  SAFETY_MODE        = 1; // potentiometers deactivated
  JOYSTICK_MODE      = 2; // bidirectional wireless link active
  NO_CONNECTION      = 3; // not used
  JOYSTICK_NO_REMOTE = 4; // ASTI controller switches in this mode, when RC connection is lost
}

enum PodCommand {
  READ_POD_STATE      = 0;
  READ_SETTINGS       = 1;
  WRITE_SETTINGS      = 2; // write pod settings
  WRITE_EL_SETTINGS   = 3; // write electrum settings
  NEW_CTRL_CMD        = 4; // new control command (steering and depower)
  READ_ELECTRUM_STATE = 5;
}

// state of GPS receiver
enum GPS_State {
  GPS_NOFIX = 0;
  GPS_FIX   = 1; // XSense
  GPS_FIX2D = 2; // not for XSense
  GPS_FIX3D = 3; // not for XSense  
}

enum ComStatePath {
  ROUTER_ROUTER      = 0;
  ROUTER_UART        = 1;
  UART_ROUTER        = 2;
  UART_UART          = 3;
}

// state of the Kalman filter;
// for visualisation, KALMAN_State and GPS_State can be added to get a single number
enum KALMAN_State {
  KALMAN_INVALID = 0;  // XSense or other Kalman estimator
  KALMAN_VALID   = 10; // XSense or other Kalman estimator
}

// Reference frames, that are used and their postfixes:
// ----------------------------------------------------
// More information can be found at http://www.kitepower.eu/wiki/index.php/Reference_frames#Kite_.28K.29
// Earth Centered Earth Fixed ECEF
// Kite Reference Frame       K
// Kite Sensor                KS
// Earth Xsense               EX (z down, x north, righthanded)
// Earth Groundstation        EG
// Wind                       W
// Small Earth                SE

// Data types and definitions for the kite state, that is determined with GPS and IMU measurements;
// this position struct is used for the position of the kite, but also for the estimated position of the ground
// station. Correct name would be PositionGEO (geodetic coordinates in ECEF reference frame).
message PositionECEF {            // position of the kite in the "Earth Centered Earth Fixed" reference frame
  optional float latitude   = 1;  // -90.0 ..90.0,     unit="deg" latitude,  angular distance of the location
                                  // south or north of the Equator
  optional float longitude  = 2;  // -180.0 .. 180.0   unit="deg" longitude, east-west position of a point on
                                  // the Earth's surface
  optional float altitude   = 3;  // -500.0 .. 5000.0  unit="m"   altitude above WGS-84 elipsoid
}

message OrientationEX {       // Euler angles in earth reference frame
  required float phi   = 1;   // roll  angle, -180.0 .. 180.0,        unit="deg"
  required float theta = 2;   // pitch angle, -90.0 ..   90.0         unit="deg"
  required float psi   = 3;   // yaw/ heading angle, -180.0 .. 180.0, unit="deg" 
}

message V_EX {            // velocity of the kite in the "Earth Xsense" reference frame
  required float vx = 1;  // velocity in direction north unit="m/s"
  required float vy = 2;  // velocity in direction east  unit="m/s"
  required float vz = 3;  // velocity in direction down  unit="m/s"
}

message V_ET {            // velocity of the kite in the "Earth Trimble" reference frame
  required float vh        = 1;  // norm of horizontal velocity; unit="m/s"
  required float ch        = 2;  // horizontal course; true north is zero; east is 90 deg; unit = "deg"
  required float vz        = 3;  // vertical velocity unit="m/s"
  optional int32 isValid   = 4;  // 1: valid, 0: invalid
  optional int32 isDoppler = 5;  // 1: computed from Doppler, 0: computed from consecutive measurements
}

message PosError {
  required float sigma_north  = 1; // unit: meters
  required float sigma_east   = 2; // unit: meters
  required float sigma_up     = 3; // unit: meters
}

message AV_KS {               // angular velocity of the kite in the "Kite Sensor" reference frame
  required float p       = 1; // roll rate,  unit="deg/s"
  required float q       = 2; // pitch rate, unit="deg/s"
  required float r       = 3; // yaw rate,   unit="deg/s"
}

message Acceleration_KS {     // acceleration of the kite in the "Kite Sensor" reference frame
  optional float ax    = 1;   // unit="m/s2"
  optional float ay    = 2;   // unit="m/s2"
  optional float az    = 3;   // unit="m/s2"
}

message KiteState {
  optional PositionECEF     pos                 = 1;    // ToDo: rename, not ECEF but geodetic reference frame
                                                        //      (spherical not rectangular)
  optional OrientationEX    orientation         = 2;
  optional V_EX             velocity            = 3;
  optional AV_KS            angular_velocity    = 4;
  optional Acceleration_KS  acc                 = 5;
  optional GPS_State        gps_state           = 6;
  optional KALMAN_State     kalman_state        = 7;
  optional PositionECEF     pos_trimble         = 8;    // from trimble message, GSOF 2: LAT, LONG, HEIGHT (p.102)
  optional V_ET             velocity_trimble    = 9;    // from trimble message, GSOF 8: VELOCITY DATA (p.108)
  optional PosError         pos_error_trimble   = 10;   // from trimble message, GSOF 12: POSITION SIGMA INFO (p.112)
  optional GPS_State        gps_state_trimble   = 11;   // from trimble message, see GSA field
  optional int32            gps_sat_trimble     = 12;   // from trimble message, GSOF 34: ALL SV DETAILED INFO
  optional int32            glos_sat_trimble    = 13;   // from trimble message, GSOF 34: ALL SV DETAILED INFO
  optional double           time_trimble        = 14;   // from trimble message, GSOF 1: UTC time in unix time format
  optional double           delay_trimble       = 15;   // time from receiving a valid trimble postion message in
                                                        //      KiteCtrlDaemon to sending it as PodState message
  optional double           delay_trimble_time  = 16;
}

message KiteCtrlState {                    // potentiometer values, representing the position of the drums with the steering and depower lines
  optional double     time            = 3; // time  of the measurement in seconds since epoch, microsecond resolution
  optional int32      steering        = 1; // range:    0 .. 4095; full left:  0 full right: 4095
  optional int32      depower         = 2; // range:    0 .. 4095; full power: 0 full depower: 4095
}

message PodSettings {
  optional int32    min_depower         = 1;  // hard-coded in the ASTI software, but different for different pods
  optional int32    max_depower         = 2;  // hard-coded in the ASTI software, but different for different pods
  optional int32    min_steering        = 3;  // hard-coded in the ASTI software, but different for different pods
  optional int32    max_steering        = 4;  // hard-coded in the ASTI software, but different for different pods
  optional int32    min_brake           = 5;  // range:    0 .. 1023; ??? hard-coded in the ASTI software, but different for different pods
  optional int32    max_brake           = 6;  // range:    0 .. 1023; ??? hard-coded in the ASTI software, but different for different pods
  optional float    full_steering       = 7;  // safe range:    0.0 .. 100.0 percent of the range, defined by max_steering and min_steering
  optional float    full_depower        = 8;  // safe range:    0.0 .. 100.0 percent of the range, defined by min_depower and max_depower
  optional float    full_power          = 9;  // safe range:    0.0 .. 100.0 percent of the range, defined by min_depower and max_depower
                                              // full_depower must be higher than full_power
  optional sint32   max_motor_temp      = 10; // maximal allowed motor temperature in degree Celsius;   range:    50..150;
  optional sint32   max_gearbox_temp    = 11; // maximal allowed gearbox temperature in degree Celsius; range:    50..150;
  optional sint32   max_controller_temp = 12; // maximal allowed motor controller temperature in degree Celsius; range:    50..150;
  optional float    battery_capacity    = 13; // nominal battery capcity in Ah; range: 0.0 to 1000.0 Ah
  optional float    weight              = 14; // weight of the pod in kg including casing, foam
}

// definitions of the data, that is transfered to monitor the health of the control pod
message PodTemperatures {                  // all temperatures in degree celsius
  optional sint32     t_smotor        = 1; // temperature of steering motor in degree Celsius
  optional sint32     t_dmotor        = 2; // temperature of depower  motor in degree Celsius
  optional sint32     t_sgearbox      = 3; // temperature of steering gearbox in degree Celsius
  optional sint32     t_dgearbox      = 4; // temperature of depower  gearbox in degree Celsius
  optional sint32     t_scontroller   = 5; // temperature of the steering controller
  optional sint32     t_dcontroller   = 6; // temperature of the depower controller
  optional sint32     t_ASTI_CPU      = 7; // temperature of the CPU of the ASTI board
  optional sint32     t_BRAINY_CPU    = 8; // temperature of the CPU of the Electrum 100 board
  optional sint32     t_BAT_01        = 9;
  optional sint32     t_BAT_02        = 10;
}

message PodElectrics {                   // electrical state of the pod; sent at about 20 Hz; measured at 200 Hz
  optional double time          = 1;     // time  of the measurement in seconds since epoch, microsecond resolution
  optional float i_max_steering = 2;     // maximal current of the steering motor in A in last period
  optional float i_max_depower  = 3;     // maximal current of the depower motor in A in last period
  optional float i_av_steering  = 4;     // average current of the steering motor in A in last period
  optional float i_av_depower   = 5;     // average current of the depower motor in A in last period
  optional float i_eff_steering = 6;     // effective current of the steering motor in A in last period
  optional float i_eff_depower  = 7;     // effective current of the depower motor in A in last period
  optional float u_av_batt      = 8;     // average battery voltage in V in last period
  optional float u_min_batt     = 9;     // minimum battery voltage in V in last period
  optional float u_av_depower   = 10;    // average voltage of depower motor controller in V in last period
  optional float u_min_depower  = 11;    // minimum voltage of depower motor controller in V in last period
  optional float u_av_steering  = 12;    // average voltage of steering motor controller in V in last period
  optional float u_min_steering = 13;    // minimum voltage of steering motor controller in V in last period
}

// data of additional sensors, that are attached to the pod
message PodSensors {                       // relative windspeed near the pod
  optional double time        = 1;         // time  of the measurement in seconds since epoch, microsecond resolution
  optional float windspeed    = 2;         // absolute value of the windspeed in m/s
  optional float force        = 3;         // absolute value of the force of the main tether in N
  optional int32 u_force      = 4;         //
  optional float u_wind       = 5;         // Unit: Volt
  optional int32 abs_pressure = 6;         // Pascal
}

// the electrum state shall be persistant;
message ElectrumSettings {
  required int32     tick_time_ms   = 1; // tick time of Brainy in ms
  optional int32     log_multiplier = 2; // the period time for logging on brainy is tick time times log_multiplier
}

// the electrum state shall be persistant;
message ElectrumState {
  optional float     jitter_ms = 1; // last jitter of the internal timer
  optional float min_jitter_ms = 2; // minimal jitter of the internal timer
  optional float max_jitter_ms = 3; // maximal jitter of the internal timer
  optional int32 disk_space    = 4; // free disk space in kB
  optional int32 mem_space     = 5; // free memory in MB
  optional float cpu_load      = 6; // cpu load in percent
}

message ComState {
  optional int32  package_no              = 1;  // sequence number of the last PodCtrl message, that was generated
                                                //  on the ground
  optional double time_sent               = 2;  // time, when the last PodCtrl message was sent from the ground
  optional int32  router_packets_received = 3;  // Number of packets received by the router
  optional int32  router_packets_sent     = 4;  // Number of packets sent by the router
  optional int32  laird_packets_received  = 5;  // Number of packets received by the Laird Module
  optional int32  laird_packets_sent      = 6;  // Number of packets sent by the Laird Module
  optional int32  path                    = 7;  // Flags with the path followed (see: enum ComStatePath)
}

// this message is sent as reply to any incoming PodCtrl message; downlink
message PodState {
  required int32 counter                   = 1; // sequence number of the package;
  optional int32 acknowledge               = 2; // sequence number of the last control command; limitied to 16 bit
  optional KiteState       kite_state      = 3; // current kite position, orientation, speed and accelleration
  optional KiteCtrlState   kite_ctrl_state = 4; // current steering and depower settings of the kite
  optional PodCtrlState    pod_ctrl        = 5; // control mode of the ASTI board (motor controller)
  optional PodTemperatures pod_temp        = 6; // temperatures of the pod
  optional PodElectrics    pod_el          = 7; // voltages and currents in the pod
  optional PodSensors      pod_sensors     = 8; // data of additional sensors, attached to the pod
  optional ElectrumState   electrum_state  = 9; // state of the Electrum 100 kite control computer
  optional ComState        com_state       = 10; // state of the communication link
  optional double          time            = 11; // time  of the measurement in seconds since epoch, microsecond
                                                 // resolution
  optional double          time_sent       = 12; // time, when the data was sent     (updated by each sender)
  optional double          time_received   = 13; // time, when the data was received (updated by each receiver)
  optional int32           crc             = 14; // crc over the message
  optional double          time_clock      = 15; // time of the FAST_CLOCK signal, that triggered this message
  optional bool            watchdog_active = 16; // must be true as long as the reset of the steering motor
                                                 //  controller is active
}

// definitions for the control commands (uplink)
message KiteCtrlCmd {                      // set values for steering and depower
  // neutral: (PodSettings.max_steering - PodSettings.min_steering) / 2
  optional int32      cmd_steering    = 1; // range:   0 .. 4095; full left:  0 full right: 4095
  optional int32      cmd_depower     = 2; // range:   0 .. 4095; full power: 0 full depower: 4095
  optional bool       reset_mc        = 3; // if true reset the steering motor controller
}

// this message is sent 20 times per second; uplink
message PodCtrl {
  required int32 counter                      = 1; // sequence number of the package; limited to 16 bit
  optional PodCommand pod_cmd                 = 2 [default=READ_POD_STATE]; // command, sent to the pod;
                                                                            // can be omitted
  optional KiteCtrlCmd kite_cmd               = 3; // set values for steering and depowering
  optional ElectrumSettings electrum_settings = 4;
  optional double time                        = 5; // time  of the control signal generation in seconds
                                                   // since epoch, microsecond resolution
  optional double time_sent                   = 6; // time, when the data was sent     (updated by each sender)
  optional double time_received               = 7; // time, when the data was received (updated by each receiver)
  optional int32 crc                          = 8; // crc over the message
  optional double time_clock                  = 9; // time of the FAST_CLOCK signal, that triggered this message
  optional bool   reset_counter               = 10; // reset the counter of the PodState messages
  optional PodSettings pod_settings           = 11; // calibration settings of the kite control unit
}

// log file messages, that are stored within Brainy
message BrainyLog {
  optional PodState pod_state         = 1; // message, received from the control pod
  optional PodCtrl  pod_ctrl          = 2; // message, sent to the control pod
  optional PodSensors pod_sensors     = 3; // measurement values of sensors at/in pod
}

// kite properties, needed for the autopilot and/or the simulation
message Kite {
  optional float weight    = 1 [default = 11.0];   // weight of the kite in kg, including the kite-bridle
  optional float ld_max    = 2 [default = 5.0];    // maximal lift over drag ration for safe operation
  optional float ld_min    = 3 [default = 1.0];    // minimal lift over drag ration for safe operation
  optional float size      = 4 [default = 25.0];   // effective size in m²
  optional float max_force = 5 [default = 4905.0]; // maximal tether force in N for safe operation
}
