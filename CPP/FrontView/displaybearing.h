/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef DISPLAYBEARING_H
#define DISPLAYBEARING_H

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsItem>
#include <QDebug>
#include <cmath>
#include <QList>
#include <QPointF>
#include <QVector>

// Responsibility: Drawing of the bearing item
class DisplayBearing : public QGraphicsItem
{
public:
    // constructor
    DisplayBearing();

    //functions
    QRectF boundingRect() const;// Implementation of a pure virtual public functions of a QGraphicsItem
                                // returns an estimate of the area painted by the item. necessary because
                                // it is called by the scene where the item is placed.

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
                                // Implementation of a pure virtual public functions of a QGraphicsItem
                                // paints subitems into the foreground item

    // setters
    void setKiteBearingParameters(QList<QPointF> m_bearing);
                                // sets the class attributes related to kiteBearing

private:
    // attributes
    QList<QPointF> m_bearing;     // List of points discribing the bearing, which is received as a
                                // vector of azimuth/elevation pairs from the controller.
};

#endif // DISPLAYBEARING_H
