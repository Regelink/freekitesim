#-------------------------------------------------
#
# Project created by QtCreator 2012-02-27T10:31:56
#
#-------------------------------------------------

QT       += core gui
QT       += network

INCLUDEPATH += /usr/local/qwt-6.1.0/include
LIBS    += /usr/local/qwt-6.1.0/lib/libqwt.so.6

CONFIG  += qwt

TARGET = FrontView
TEMPLATE = app

PROTOS = ../ProtoBuf/asset.pod.proto \
         ../ProtoBuf/asset.winch.proto \
         ../ProtoBuf/asset.system.proto

include(../ProtoBuf/protobuf.pri)

SOURCES += main.cpp\
        frontview.cpp\
        ../ZeroMQLink/zeromqlink.cpp \
        ../ZeroMQLink/abstractlink.cpp \
    displaybackground.cpp \
    frontviewsettings.cpp \
    displayredceiling.cpp \
    displaytrajectory.cpp \
    displaybearing.cpp \
    displaykite.cpp

HEADERS  += frontview.h\
        ../ZeroMQLink/zeromqlink.h \
        ../ZeroMQLink/abstractlink.h \
    displaybackground.h \
    frontviewsettings.h \
    displayredceiling.h \
    displaytrajectory.h \
    displaybearing.h \
    displaykite.h

FORMS    += frontview.ui

LIBS    += /usr/lib/libprotobuf.so \
        -lzmq

include(../QsLog/QsLog.pri)
include(../Utils/utils.pri)

OTHER_FILES += \
    ../ProtoBuf/asset.pod.proto \
    ../ProtoBuf/asset.system.proto \
    ../ProtoBuf/asset.winch.proto \
























