/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "displaytrajectory.h"
#include "QsLog.h"

DisplayTrajectory::DisplayTrajectory()
{
    QLOG_DEBUG() << "Created DisplayTrajectory...";
    m_trajectoryPoints.append(QPointF(0.0, 0.0));
}

QRectF DisplayTrajectory::boundingRect() const
{
    return QRectF(-280, -150, 560, 300);
}

void DisplayTrajectory::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setClipRect( option->exposedRect );
    (void) widget;
    painter->translate(0, 125);
    QPen magentaPen(Qt::magenta, m_pointSize, Qt::SolidLine, Qt::RoundCap);
    painter->setPen(magentaPen);

    if (m_trajectoryPoints.size() >= 100) {
        for (int i = 0; i < m_trajectoryPoints.size(); i = i + 10) {
            painter->drawPoint(m_trajectoryPoints[i]);
        }
    } else {
        for (int j = 0; j < m_trajectoryPoints.size(); j++) {
            painter->drawPoint(m_trajectoryPoints[j]);
        }
    }
    QLOG_DEBUG() << "DisplayTrajectory: painted...";
    //qDebug() << "DisplayTrajectory: painted...";
}

void DisplayTrajectory::setDesiredTrajectoryParameters(QList<QPointF> trajectoryPoints, int pointSize)
{
    prepareGeometryChange();
    m_trajectoryPoints = trajectoryPoints;
    m_pointSize = pointSize;
    QLOG_DEBUG() << "DisplayTrajectory: flight trajectory updated...";
}

void DisplayTrajectory::setTrajectoryPointSize(int pointSize)
{
    prepareGeometryChange();
    m_pointSize = pointSize;
    QLOG_DEBUG() << "DisplayTrajectory: flight trajectory point size updated...";
}

void DisplayTrajectory::removeDesiredTrajectory()
{
    prepareGeometryChange();
    m_trajectoryPoints.clear();
}
