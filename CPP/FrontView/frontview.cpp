/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "frontview.h"
#include "ui_frontview.h"
#include "QsLog.h"
#include <QCoreApplication>

FrontView::FrontView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FrontView)
{
    ui->setupUi(this);
    setupSettings();
    setupGui();
    setupCommunication();
    QTimer::singleShot(200, this, SLOT(afterStartUp()));
}

FrontView::~FrontView()
{
    delete ui;
}

void FrontView::closeEvent(QCloseEvent *event)
{
    m_settings->write();
    m_settings->getSettings()->setValue("geometry", saveGeometry());
    QWidget::closeEvent(event);
}
void FrontView::setupSettings()
{
    // load settings file.
    m_cfilename = "FrontView";
    if (QCoreApplication::arguments().count() > 1) {
        m_cfilename=QCoreApplication::arguments().at(1);
    }
    setWindowTitle(m_cfilename);
    m_settings = new FrontViewSettings(m_cfilename);

    // Retrive values from settings
    m_trailLength  = m_settings->getTrailLength();
    m_headingScale = m_settings->getHeadingScale();
    m_courseScale  = m_settings->getCourseScale();
    m_ceilingHeight = m_settings->getCeilingHeight();
    m_trajectoryPointSize = m_settings->getTrajectoryPointSize();
}

void FrontView::setupGui()
{

    // lock aspect ratio and restore window size and position from last session
    FrontView::sizePolicy().setHeightForWidth(true);
    FrontView::restoreGeometry(this->m_settings->getSettings()->value("geometry").toByteArray());
    ui->graphicsView->setOptimizationFlag(QGraphicsView::DontSavePainterState,true);
    ui->graphicsView->setAttribute(Qt::WA_TranslucentBackground, false);

    // add menu options
    m_viewOptions = menuBar()->addMenu("&View");
    m_setTrajectoryPointSize = new QAction("&Trajectory size",this);
    connect(m_setTrajectoryPointSize,SIGNAL(triggered()), this, SLOT(changeTrajectoryPointSize()));
    m_viewOptions->addAction(m_setTrajectoryPointSize);
    m_setTrailLength = new QAction("&Trail length",this);
    connect(m_setTrailLength,SIGNAL(triggered()), this, SLOT(changeTrailLength()));
    m_viewOptions->addAction(m_setTrailLength);

    // add scenes to view, fitting the scene size to view size and add items to scene
    m_scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(m_scene);

    m_redCeilingDisplay = new DisplayRedCeiling();
    ui->graphicsView->fitInView(m_redCeilingDisplay, Qt::KeepAspectRatio);
    m_scene->addItem(m_redCeilingDisplay);

    m_backgroundDisplay = new DisplayBackground();
    ui->graphicsView->fitInView(m_backgroundDisplay, Qt::KeepAspectRatio);
    m_backgroundDisplay->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
    m_scene->addItem(m_backgroundDisplay);

    m_trajectoryDisplay = new DisplayTrajectory();
    ui->graphicsView->fitInView(m_trajectoryDisplay, Qt::KeepAspectRatio);
    m_scene->addItem(m_trajectoryDisplay);

    m_bearingDisplay = new DisplayBearing();
    ui->graphicsView->fitInView(m_bearingDisplay, Qt::KeepAspectRatio);
    m_scene->addItem(m_bearingDisplay);

    m_kiteStateDisplay = new DisplayKite();
    ui->graphicsView->fitInView(m_kiteStateDisplay, Qt::KeepAspectRatio);
    m_scene->addItem(m_kiteStateDisplay);

    // update qGraphicsView widget size (to overcome the small intial size)
    ui->graphicsView->setAlignment(Qt::AlignCenter);
    ui->graphicsView->resize((FrontView::width()-20), (FrontView::height() - 40));
    m_kiteStateDisplay->setKiteTrailSize(m_settings->getTrailLength());
}

void FrontView::setupCommunication()
{
    //setup and connect zeroMQLink
    m_linkTimer = new QTimer;
    m_zeroMQLink = new ZeroMQLink();
    QStringList inputSources = m_settings->getInputSources().split(' ');
    QLOG_INFO() << "input_sources: " << inputSources;
    for (int i = 0 ; i < inputSources.length() ; i++) {
        QString inputSource  = inputSources[i];
        int result = m_zeroMQLink->connect_link(inputSource);
        QLOG_INFO() << "Listening on: " << inputSource << "Result: " << result;
    }
    m_zeroMQLink->subscribe(asset::system::mtEstimatedSystemState);
    m_zeroMQLink->subscribe(asset::system::mtDesiredTrajectory);
    m_zeroMQLink->subscribe(asset::system::mtBearing);
    m_zeroMQLink->subscribe(asset::system::mtEventMsg);
    connect(m_zeroMQLink, SIGNAL(messageReceived(int)), this, SLOT(handleMessageReceived(int)));
    m_zeroMQLink->start();
}

void FrontView::handleMessageReceived(int msg_id)
{
    if (msg_id == asset::system::mtEstimatedSystemState){
        onEstimatedsystemStateMessage();
    } else if (msg_id == asset::system::mtDesiredTrajectory) {
        onDesiredTrajectoryMessage();
    } else if (msg_id == asset::system::mtBearing){
        onBearingMessage();
    } else if (msg_id == asset::system::mtEventMsg){
        onEventMessage();
    }
}

void FrontView::onEstimatedsystemStateMessage()
{
    m_message = m_zeroMQLink->getLastMessage(asset::system::mtEstimatedSystemState);
    m_systemStateMsg.Clear();
    if (!m_systemStateMsg.ParseFromArray(m_message.data(), m_message.size())) {
        QLOG_ERROR() << "Corrupted system state message.";
    } else {
        if (m_systemStateMsg.has_azimuth() && m_systemStateMsg.has_elevation() &&
                m_systemStateMsg.has_heading() && m_systemStateMsg.has_course() &&
                m_systemStateMsg.has_heading_d() && m_systemStateMsg.has_course_d() &&
                m_systemStateMsg.has_tether_length_reelout()) {
            QLOG_DEBUG() << "Recived system state parameters.";
            QPointF kitePosition = xyCoordinates(m_systemStateMsg.elevation() ,
                                                 m_systemStateMsg.azimuth());
            QPointF headingPoint = headingCoordinates(m_systemStateMsg.elevation(), m_systemStateMsg.azimuth(),
                                                      m_systemStateMsg.heading_d().hx(),
                                                      m_systemStateMsg.heading_d().hy());
            QPointF coursePoint = courseCoordinates( m_systemStateMsg.elevation(),  m_systemStateMsg.azimuth(),
                                                     m_systemStateMsg.course_d().cx(),
                                                     m_systemStateMsg.course_d().cy());
            double heading =    m_systemStateMsg.heading();
            double course  =    m_systemStateMsg.course();
            double height = this->setCeilingParameters(m_systemStateMsg.tether_length_reelout() ,
                                                            m_settings->getCeilingHeight());
            m_redCeilingDisplay->setCeilingParameters( height);
            m_kiteStateDisplay->setKiteStateParameters(kitePosition, headingPoint, coursePoint, heading, course);
        }
    }
}

void FrontView::onDesiredTrajectoryMessage()
{
    m_message = m_zeroMQLink->getLastMessage(asset::system::mtDesiredTrajectory);
    m_desiredTrajectoryMsg.Clear();
    if (!m_desiredTrajectoryMsg.ParseFromArray(m_message.data(), m_message.size())) {
        QLOG_ERROR() << "Corrupted Desired Trajectory message.";
        QLOG_DEBUG() << "Corrupted Desired Trajectory message.";
    } else {

        if (m_desiredTrajectoryMsg.has_delete_trajectory() && m_desiredTrajectoryMsg.delete_trajectory() == true) {
            m_trajectoryDisplay->removeDesiredTrajectory();
            QLOG_DEBUG() << "flightTrajectory removed";
            m_trajectoryDisplay->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
        }

        if (m_desiredTrajectoryMsg.trajectory_size() >= 1) {
            QList<QPointF> trajectoryPoints;
            int i = 0;
            for (; i < m_desiredTrajectoryMsg.trajectory_size(); i++) {
                trajectoryPoints.append(xyCoordinates(m_desiredTrajectoryMsg.trajectory(i).elevation(),
                                               m_desiredTrajectoryMsg.trajectory(i).azimuth()));
                qDebug() << m_desiredTrajectoryMsg.trajectory(i).elevation();
                qDebug() << m_desiredTrajectoryMsg.trajectory(i).azimuth();
            }
            QLOG_DEBUG() << "Recived flightTrajectory parameters:" << i;
            m_trajectoryDisplay->setDesiredTrajectoryParameters(trajectoryPoints, m_trajectoryPointSize);
            m_trajectoryDisplay->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
        }
    }
}

void FrontView::onBearingMessage()
{
    m_message = m_zeroMQLink->getLastMessage(asset::system::mtBearing);
    m_bearingMsg.Clear();

    if (!m_bearingMsg.ParseFromArray(m_message.data(), m_message.size())) {
        QLOG_ERROR() << "Corrupted bearing message.";
    } else if (m_bearingMsg.bearing_size() >= 1) {
        QList<QPointF> bearingPoints;
        for (int i = 0 ; i < m_bearingMsg.bearing_size() ; i++) {
            bearingPoints.append(xyCoordinates(m_bearingMsg.bearing(i).elevation(),
                                               m_bearingMsg.bearing(i).azimuth()));
        }
        QLOG_DEBUG() << "Recived bearing parameters.";
        m_bearingDisplay->setKiteBearingParameters(bearingPoints);
    }
}

void FrontView::onEventMessage()
{
    QLOG_INFO() << "onEventMessage()";
    m_message = m_zeroMQLink->getLastMessage(asset::system::mtEventMsg);
    m_eventMsg.Clear();

    if (!m_eventMsg.ParseFromArray(m_message.data(), m_message.size())) {
        QLOG_ERROR() << "Corrupted event message.";
    } else {
        if ((m_eventMsg.event() == asset::system::CLOSE) && (m_eventMsg.program() == asset::system::prgFrontView)){
            QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
        }
        if (m_eventMsg.event() == asset::system::START && m_kiteStateDisplay){
            m_kiteStateDisplay->clear();
        }
    }
}

QPointF FrontView::xyCoordinates(double elevation, double azimuth)
{
    QPointF xyCoordinates;
    xyCoordinates.setX( 250 * sin(azimuth) * cos(elevation));
    xyCoordinates.setY( - 250 * sin( elevation));
    return xyCoordinates;
}

QPointF FrontView::headingCoordinates(double elevation, double azimuth, double hx, double hy)
{
    QPointF heading;
    double vectorNorm = sqrt(pow(hx, 2) + pow(hy, 2));
    heading.setX(hx * m_headingScale / vectorNorm);
    heading.setY(hy * m_headingScale / vectorNorm);
    heading += xyCoordinates(elevation, azimuth);
    return heading;
}

QPointF FrontView::courseCoordinates(double elevation, double azimuth, double cx, double cy)
{
    QPointF course;
    double vectorNorm = sqrt(pow(cx,2) + pow(cy,2));
    course.setX( cx * m_courseScale / vectorNorm);
    course.setY( cy * m_courseScale / vectorNorm);
    course += xyCoordinates(elevation, azimuth);
    return course;
}

void FrontView::resizeEvent(QResizeEvent *event)
{
    (void) event;
    FrontView::resize(FrontView::width(),FrontView::heightForWidth(FrontView::width()));
    ui->graphicsView->resize((FrontView::width()-20),(FrontView::height()-50));
    ui->graphicsView->fitInView(m_redCeilingDisplay, Qt::KeepAspectRatio);
    ui->graphicsView->fitInView(m_backgroundDisplay, Qt::KeepAspectRatio);
    ui->graphicsView->fitInView(m_trajectoryDisplay, Qt::KeepAspectRatio);
    ui->graphicsView->fitInView(m_bearingDisplay,    Qt::KeepAspectRatio);
    ui->graphicsView->fitInView(m_kiteStateDisplay,  Qt::KeepAspectRatio);
}

int FrontView::heightForWidth(int w)
{
    int height = qRound(w * 0.5);
    return (height);
}

double FrontView::setCeilingParameters(double cableLength, double ceilingHeight)
{
    double height;
    if (cableLength <= 0 || cableLength <= ceilingHeight) {
        height = 250;
    } else {
        height = 250 * sin((M_PI / 2) - (acos(ceilingHeight / cableLength)));
    }
    return height;
}

void FrontView::changeTrajectoryPointSize()
{
    bool ok;
    int size=QInputDialog::getInt(this, "Input trajectory point size", "Front View",
                                  m_trajectoryPointSize, 0, 10, 1, &ok);
    if(ok && (size != m_trajectoryPointSize)) {
        m_trajectoryPointSize = size;
        m_settings->setTrajectoryPointSize(size);
        m_trajectoryDisplay->setTrajectoryPointSize(size);
    }
}

void FrontView::changeTrailLength()
{
    bool ok;
    int length = QInputDialog::getInt(this, "Input trail length", "Front View", m_trailLength, 0, 10000, 1, &ok);
    if(ok && length != m_trailLength) {
        m_trailLength = length;
        m_settings->setTrailLength(length);
        m_kiteStateDisplay->setKiteTrailSize(length);
    }
}

void FrontView::afterStartUp()
{
    FrontView::restoreGeometry(this->m_settings->getSettings()->value("geometry").toByteArray());
    if (pos().y() == 0) {
        move(pos().x(), -8);
    }
    Utils::usleep(50000);
    QCoreApplication::postEvent(this, new QEvent(QEvent::Resize));
}
