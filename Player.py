"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
Player for log files, created with KiteSim or FreeKiteSim such that the kite state can be viewed in the
3D KiteViewer, but also in FrontView.
"""

import pandas as pd
import time as ti
import math
from Publisher import Publisher
from Settings import BUSY_WAITING, PERIOD_TIME
from pyqtgraph.Qt import QtCore
import matplotlib.pyplot as plt

PLAY = True # play and publish the content of the logfile for the 3D viewer
PLOT = True # plot height and power vs. time, using matplotlib

T_START = 10  # start time in seconds
T_STOP  = 20 # stop time in seconds

# log-files are recorded in the folder ./00_results
# log_file = "log.df"
log_file = "log_8700W_8ms.df" # 8700W average mechanical cylce power, 8 m/s wind at 6 m height

DELTA = 10e-4

class Player(object):
    """ This class provides methods to publish log files, that are stored as pandas data-frames using
    google-protocol-buffer encoded messages over an zeromq socket. The format of the messages is
    defined in the file './asset/asset.system.proto' ."""
    def __init__(self, filename = None):
        if filename is not None:
            self.df = pd.read_pickle(filename)
        self.pub = None
        self.stopped = False

    def read(self, filename):
        self.df = pd.read_pickle(filename)

    def publish(self, rel_start=T_START, rel_stop=T_STOP, parent = None):
        """ Publish the ParticleSystem messages for the time from rel_start to rel_stop.
        The relative start and stop times have to be given in seconds. """
        self.pub = Publisher()
        self.stopping = False
        t_launch, last_time = 0.0, 0.0
        df1 = self.df[(self.df['time_rel'] - t_launch > rel_start)
                                & (self.df['time_rel'] - t_launch < rel_stop)]
        try:
            for date, row in df1.T.iteritems():
                if self.stopping:
                    break
                if BUSY_WAITING:
                    i = 0
                    # allow a jitter of 500 micro seconds
                    while (math.fabs(math.fmod(ti.time(), PERIOD_TIME)) > 5e-4):
                        i += 1
                else:
                    # allow a jitter of 500 micro seconds
#                    delta = math.fabs(math.fmod(ti.time(), PERIOD_TIME))
#                    if delta > DELTA:
#                        ti.sleep(delta - DELTA)
                    while (math.fabs(math.fmod(ti.time(), PERIOD_TIME)) > DELTA / 2.0):
                        ti.sleep(DELTA / 2.0)
                time = row['time_rel']
                v_reel_out = row['v_reelout']
                pos = row['position']
                v_app = row['v_app']
                l_tether = row['l_tether']
                force = row['force']
                azimuth = row['azimuth']
                elevation = row['elevation']
                vel_kite = row['vel_kite']
                try:
                    prediction = row['prediction']
                except:
                    prediction = []

                # print time, pos, v_reel_out, v_app, l_tether, force, azimuth, elevation, vel_kite
                if parent is not None:
                    if time > last_time + 0.09:
                        parent.emit(QtCore.SIGNAL("update(float)"), time)
                        last_time = time
                else:
                    print time
                self.pub.publishParticleSystem(pos, v_reel_out, v_app, l_tether, force, azimuth, elevation, \
                                    vel_kite, prediction)
        finally:
            self.pub.close()
        return

    def stop(self):
        self.stopping = True

if __name__ == '__main__':
    player = Player('./00_results/'+log_file)
    df = player.df
    if PLAY:
        player.publish()
    if PLOT:
        df.height.plot()
        plt.figure()
        df.power = df.v_reelout * df.force
        df.power.plot()
