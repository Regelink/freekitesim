# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
GUI for Logfile Player

If launched with Spyder select: "Execute in new, dedicated Python interpreter. "
Tutorial for PyQt: http://zetcode.com/tutorials/pyqt4/
"""

import sys
import os
import glob
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from Player import Player
import pandas as pd
import numpy as np
from math import pi

# translate field names
LABELS = {
         "gps_state"   : "Gps State []",
         "tetherLength": "Tether length [m] ",
         "height":"Height [m]",
         "force":"Force [N]",
         "v_reelout":"Reel-out speed [m/s]",
         "t_smotor":u"Temp. steering motor [°C]",
         "t_dmotor":u"Temp. depower motor [°C]",
         "app_wind_kite":"Apparent air speed at the kite [m/s]",
         "alt":"Altitude from XSense [m]",
         "elevation":u"Elevation angle [°]",
         "azimuth":u"Azimuth angle [°]",
         "speed_xsens":"Kite speed (XSense GPS) [m/s]",
         "speed_trimble":"Kite speed (Trimble GNSS) [m/s]",
         "mech_power":"Mechanical power [W]",
         "v_app_norm":"Apparent air speed [m/s]",
         "sync_speed":"Synchronous speed [m/s]",
         "set_force": "Set force [N]",
         "gps_sat_trimble":"GPS satellites",
         "acc_xsens":u"||Acceleration|| [m/s²]",
         "kite_distance":"Kite distance [m]",
         "tether_length_reelout":"Reel-out length [m]",
         "sigma_up":"Standart deviation in z-direction [m]",
         "xsens_kite_distance":"Kite distance by XSens MTi-G [m]",
         "rel_steering":"Relative steering",
         "rel_d_psi":"Relative yaw rate [rad/m]",
         "rel_yaw_rate":"Relative yaw rate [rad/m]",
         "course":"Course angle [°]",
         "heading":"Heading angle [°]",
         "steering":"Relative steering (u_s)",
         "depower":"Relative depower (u_d)",
         "system_state":"System state",
         "yaw_angle":"Yaw angle [°]",
         "turn_rate":"Turn rate [°/s]"
         # TODO: add all the other name value pairs
         }

def lookup(name):
    """
    Look up the name of a field in the dictionary LABELS;
    if it is not contained in the dictionary, return the field name itself.
    """
    try:
        result = LABELS[name]
    except:
        result = name
    return result

class PlayerThread(QtCore.QThread):
    """ Worker thread to run the player in the background, while keepint the GUI responsive. """
    def __init__(self, parent = None):
        QtCore.QThread.__init__(self, parent)
        self.player = Player()

    def play(self, file_name, start, stop):
        """ Start playing the given logfile. """
        self.player.read(file_name)
        self.start_time = start
        self.stop_time = stop
        self.start()

    def run(self):
        """ This run method is called by Qt as a result of calling start(). """
        print "Starting the ponderous task"
        self.player.publish(self.start_time, self.stop_time, self)
        print "Finished the ponderous task"

    def stop(self):
        """ Stop playing. Must not be called directly, but only via a signal. """
        self.player.stop()

class GUI_Player(QtGui.QWidget):
    """ This class implements a QT-based graphical user interface (GUI) for the log-file player as
    implemented in the module Player.py ."""
    def __init__(self):
        super(GUI_Player, self).__init__()
        self.initUI()
        self.player_thread = PlayerThread()
        self.last_line = None
        self.begin = -100.0
        self.end = 1e6
        self.pause = True
        self.simul_time = 0.0
        # connect the stop method of the GUI with the stop method of the player thread
        self.connect(self, QtCore.SIGNAL("cancelThread()"), self.player_thread.stop)
        self.connect(self.player_thread, QtCore.SIGNAL("update(float)"), self.updateSimulTime)

    def initUI(self):
        """ Initialize the user interface (UI). """
        # set window size and title
        self.setGeometry(300, 300, 600, 450)
        self.setWindowTitle('Logfile Player')

        # create pg graphics view
        plot = pg.PlotWidget(self, useOpenGL = False)
        plot.setAntialiasing(True)
        self.plot = plot # make plot accessable from other methods

        # create combo box to select log-file
        combo = QtGui.QComboBox(self)
        path = os.path.dirname(os.path.realpath(__file__))
        os.chdir(path+"/00_results")
        self.log_path = path + "/00_results/"

        # fill the combo box with the names of the log-files
        for file_name in glob.glob("*.df"):
            combo.addItem(file_name)

        # restore the current directory
        os.chdir(path)
        combo.move(20, 30)

        # create a combo box to select the column
        self.combo_clm = QtGui.QComboBox(self)

        # save current file name
        self.logfile_name = combo.currentText()

        # create buttons to play the logfile
        btn0 = QtGui.QPushButton('Load', self)
        btn1 = QtGui.QPushButton('Play', self)
        btn2 = QtGui.QPushButton('Pause', self)
        self.btn2 = btn2
        btn3 = QtGui.QPushButton('Stop', self)

        # create line edits for start time, stop time and simulation time
        lbl0 = QtGui.QLabel()
        lbl0.setText('Start time [s]')
        self.qleStart = QtGui.QLineEdit()
        self.qleStart.setFixedWidth(95)
        lbl1 = QtGui.QLabel()
        lbl1.setText('Stop time [s]')
        self.qleStop = QtGui.QLineEdit()
        self.qleStop.setFixedWidth(95)
        lbl2 = QtGui.QLabel()
        lbl2.setText('Simul. time [s]')
        self.qleSimulTime = QtGui.QLineEdit()
        self.qleSimulTime.setFixedWidth(95)
        lbl3 = QtGui.QLabel()
        lbl3.setText('Field: ')

        # create the layout
        # create a hbox layout for label and the combobox for the fields
        hbox1 = QtGui.QHBoxLayout()
        hbox1.addWidget(lbl3)
        hbox1.addWidget(self.combo_clm)

        # create a vbox layout for combobox and plot-window
        vbox0 = QtGui.QVBoxLayout()
        vbox0.addWidget(combo)
        vbox0.addLayout(hbox1)
        vbox0.addWidget(plot)

        # create a vbox layout for the buttons
        spacer = QtGui.QSpacerItem(20, 1600, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Expanding)
        vbox1 = QtGui.QVBoxLayout()
        vbox1.addStretch(1)
        vbox1.addWidget(btn0)
        vbox1.addWidget(btn1)
        vbox1.addWidget(btn2)
        vbox1.addWidget(btn3)
        vbox1.addSpacerItem(spacer)
        vbox1.addWidget(lbl0)
        vbox1.addWidget(self.qleStart)
        vbox1.addWidget(lbl1)
        vbox1.addWidget(self.qleStop)
        vbox1.addSpacerItem(spacer)
        vbox1.addWidget(lbl2)
        vbox1.addWidget(self.qleSimulTime)
        #vbox1.addSpacerItem(spacer)

        # create a hbox of both vboxes
        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(vbox0)
        hbox.addLayout(vbox1)
        self.setLayout(hbox)

        # connect the buttons, combo box, line edits and plot window
        btn0.clicked.connect(self.load)
        btn1.clicked.connect(self.play)
        btn2.clicked.connect(self.pause_continue)
        btn3.clicked.connect(self.stop)
        combo.activated[str].connect(self.file_selected)
        self.combo_clm.activated[str].connect(self.column_selected)
        self.qleStart.textChanged[str].connect(self.setStartTime)
        self.qleStop.textChanged[str].connect(self.setStopTime)
        self.plot.sigXRangeChanged.connect(self.timeRangeChanged)

        # show the GUI
        self.show()

    def file_selected(self, text):
        """ Handler for an event from the combobox. """
        self.logfile_name = text
        self.load()

    def column_selected(self, text):
        self.plot.clear()
        if str(text) in ('azimuth','course','elevation','heading','yaw_angle', 'turn_rate'):
            self.plot.plot(self.df.time_rel, self.df[str(text)] * 180.0/pi, pen=(255, 0, 0))
        else:
            self.plot.plot(self.df.time_rel, self.df[str(text)], pen=(255, 0, 0))
        self.plot.setLabel('left', lookup(str(text)))
        self.last_line = None

    def load(self):
        print "Loaded: ", self.log_path + self.logfile_name
        self.df = pd.read_pickle(self.log_path + self.logfile_name)
        self.column_selected('height')
        self.plot.plot(self.df.time_rel, self.df.height, pen=(255, 0, 0))
        self.plot.setLabel('bottom', "Time [s]")
        self.begin = self.df.time_rel.min()
        self.end = self.df.time_rel.max()
        if ("turn_rate" in self.df.columns.tolist()):
            (self.df.turn_rate[(abs(self.df.turn_rate) > 2.0)]) = np.nan
        i = 0
        self.combo_clm.clear()
        for column in self.df.columns:
            column_type = type(self.df[str(column)].values[0])
            if column_type == np.float64 or column_type == np.int64:
                if column != 'time' and column != 'time_rel':
                    self.combo_clm.addItem(column)
            else:
                print 'type:', type(self.df[str(column)].values[0])
            if column == 'height': # the default column shall be height
                index = i
            i+=1
        self.combo_clm.setCurrentIndex(index)
        self.lastStartTime = self.begin
        self.lastStopTime = self.end
        self.plot.enableAutoScale()
        self.qleStart.setText(str(self.begin))
        self.qleStop.setText(str(self.end))
        self.last_line = None

    def timeRangeChanged(self):
        time_range = self.plot.getViewBox().viewRange()[0]
        print 'time_range ', time_range
        self.qleStart.setText(str(round(time_range[0], 2)))
        self.qleStop.setText(str(round(time_range[1], 2)))

    def setStartTime(self, rel_time):
        try:
            rel_time = float(rel_time)
        except:
            self.qleStart.setText(str(self.lastStartTime))
            return
        print 'rel_time: ', rel_time, type(rel_time)
        if rel_time >= self.begin and rel_time <= self.end:
            self.lastStartTime = rel_time
            print 'New start time: ', rel_time
        else:
            self.qleStart.setText(str(self.lastStartTime))

    def setStopTime(self, rel_time):
        try:
            rel_time = float(rel_time)
        except:
            self.qleStop.setText(str(self.lastStopTime))
            return
        print 'rel_time: ', rel_time, type(rel_time), self.begin, self.end
        if rel_time >= self.begin and round(rel_time, 2) <= round(self.end, 2):
            self.lastStopTime = round(rel_time, 2)
            print 'New stop time: ', rel_time
        else:
            self.qleStop.setText(str(self.lastStopTime))

    def play(self):
        if self.lastStartTime >= self.lastStopTime:
            print "Stop time must be greater than start time!"
            return
        print "Playing: ", self.log_path + self.logfile_name
        self.btn2.setText('Pause')
        self.pause = True
        self.player_thread.play(self.log_path + self.logfile_name, self.lastStartTime, self.lastStopTime)

    def pause_continue(self):
        if self.pause:
            print 'Pause...'
            self.btn2.setText('Continue')
            self.pause = False
            self.stop()
        else:
            print 'Continue...'
            self.btn2.setText('Pause')
            self.pause = True
            self.player_thread.play(self.log_path + self.logfile_name, self.simul_time, self.lastStopTime)

    def updateSimulTime(self, simul_time):
        self.simul_time = round(simul_time, 1)
        self.qleSimulTime.setText(str(self.simul_time))
        if self.last_line is not None:
            self.last_line.setValue(simul_time)
        else:
            pen = pg.mkPen(width=1.5, color='y')
            self.last_line=self.plot.addLine(simul_time, pen = pen)
            # self.last_line.setMovable(True)

    def stop(self):
        self.emit(QtCore.SIGNAL("cancelThread()"))
        print "Emitted stop signal."

def main():
    app = QtGui.QApplication(sys.argv)
    player = GUI_Player()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
