# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
import time
try:
    import pygame
    PYGAME = True
except:
    PYGAME = False

LOGITECH = False # default: no kite joystick present
THROTTLE = False # default: no winch joystick present
RUMBLEPAD = False # default: no kite joystick present

if PYGAME:
    pygame.joystick.init()
    print "Number of joysticks / pads found: ", pygame.joystick.get_count()
    if pygame.joystick.get_count() > 1:
        joy = pygame.joystick.Joystick(1)
        name = joy.get_name()
        print "Joystick name: ", name
        joy.init()
        if name == 'Logitech Logitech Extreme 3D':
            LOGITECH = True
            Kitejoy = joy
        elif name == 'CH PRODUCTS CH PRO THROTTLE USB ':
            THROTTLE = True
            Winchjoy = joy

    if pygame.joystick.get_count() > 0:
        joy = pygame.joystick.Joystick(0)
        joy.init()
        name = joy.get_name()
        print "Joystick name: ", name
        if name == 'Logitech Logitech Extreme 3D':
            LOGITECH = True
            Kitejoy = joy
            if not THROTTLE:
                Winchjoy = joy
        elif name == 'CH PRODUCTS CH PRO THROTTLE USB ':
            THROTTLE = True
            Winchjoy = joy
        elif name == 'Logitech Logitech Cordless RumblePad 2':
            RUMBLEPAD = True
            Kitejoy = joy
            Winchjoy = joy
            print "Rumplepad chosen"
        else :
            RUMBLEPAD = False
            Kitejoy = joy
            Winchjoy = joy
            print "No known joystick type found!"
        pygame.init()


def joystickPresent():
    if PYGAME:
        return LOGITECH or THROTTLE or RUMBLEPAD
    else:
        return False

def getSetValue():
    for event in pygame.event.get(): # User did something
        pass
    value = Winchjoy.get_axis(2)
    if THROTTLE:
        value -= (0.866-3.5e-5)
        if value >= 0:
            value = 0
        value /= -(1.866-3.5e-5)
        if value  <= 0.006:
            value = 0
        if Winchjoy.get_button(13):
            return -value
    return value

def getSetValues():
    """ return the set value for the winch (-1.0..1.0)
        and the set value for the steering (-1.0..1.0)
    """
    for event in pygame.event.get(): # User did something
        pass
    # range: 0.866/0.876 to -1.0
    if LOGITECH:
        f_winch = Winchjoy.get_axis(2)
    if RUMBLEPAD:    
        f_winch = Winchjoy.get_axis(4)
    if THROTTLE:
        f_winch -= (0.866-3.5e-5)
        if f_winch >= 0:
            f_winch = 0
        f_winch /= -(1.866-3.5e-5)
        if f_winch  <= 0.006:
            f_winch = 0
        # button 13/ 15: 0 = reel-out; 1 = reel-in
        if Winchjoy.get_button(13):
            f_winch *= -1
    if LOGITECH:
        steering = Kitejoy.get_axis(0)
    elif RUMBLEPAD:
        steering = Kitejoy.get_axis(0) /8
        # f_winch -= (0.866-3.5e-5)
        #if f_winch >= 0:
        #    f_winch = 0
        # f_winch /= -(1.866-3.5e-5)
        #if f_winch  <= 0.006:
        #    f_winch = 0
        # button 13/ 15: 0 = reel-out; 1 = reel-in
        #if Winchjoy.get_button(6):
        #    print "winchbutton pressed"
        #    f_winch *= -1  
        #print "f_winch:", f_winch
        f_winch = -f_winch
        #if Winchjoy.get_button(0):
        #   print "button 0 pressed"   
    else:
        steering = 0.0
    return f_winch, steering

def getStart():
    for event in pygame.event.get(): # User did something
        pass
    if Winchjoy.get_button(0):
        print "button 0 pressed" 
        joystart = True
    else:
        joystart = False
    return joystart

if __name__ == '__main__':
    if joystickPresent():
        for i in range(500):
            for event in pygame.event.get(): # User did something
                pass
            f_winch, steering = getSetValues()
            print f_winch, steering
            time.sleep(0.05)



