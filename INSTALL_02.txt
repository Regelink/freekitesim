Installation instruction for numba for Ubuntu 12.04 (32 or 64 bit)
==================================================================

For running the dynamic simulation, the numba compiler in version 0.11.1 is needed.

Installing Numba (just-in-time compiler for Python)
===================================================

Make sure, that numpy version 1.7.1 or 1.8.1 is installed. You can check this from the python console with the commands:

import numpy
print numpy.__version__

http://numba.pydata.org/install.html

cd ~
mkdir 00Software
cd 00Software
wget http://llvm.org/releases/3.2/llvm-3.2.src.tar.gz
tar zxvf llvm-3.2.src.tar.gz
mv llvm-3.2.src llvm-3.2
mkdir build
cd build
../llvm-3.2/configure --enable-optimized --prefix=/opt --enable-targets=host
REQUIRES_RTTI=1 make -j3
make check-all
sudo REQUIRES_RTTI=1 make install

cd ..
git clone git://github.com/llvmpy/llvmpy.git
cd llvmpy
git checkout 0.11.1
sudo LLVM_CONFIG_PATH=/opt/bin/llvm-config python setup.py install

exit terminal; start terminal

python -c "import llvm; llvm.test()"

cd ~/00Software
sudo apt-get install libffi-dev
git clone https://github.com/numba/numba.git
cd numba
git checkout 0.11.1
sudo pip install -r requirements.txt
sudo python setup.py install

Installation instructions for Assimulo for Ubuntu 12.04
-------------------------------------------------------

sudo apt-get install openmpi-bin openmpi-doc libopenmpi-dev
sudo apt-get install gfortran
sudo apt-get install libblas-dev liblapack-dev

cd ~/00Software
wget https://dl.dropboxusercontent.com/u/9075004/sundials-2.5.0.tar.gz
tar xzf sundials-2.5.0.tar.gz
cd sundials-2.5.0
./configure F77=gfortran --with-blas --with-lapack --with-cflags="-march=native -O2 -fPIC" 
make -j4
sudo make install

sudo pip uninstall cython
sudo apt-get install cython
sudo pip install -U assimulo