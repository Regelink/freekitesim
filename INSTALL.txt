Installation of FreeKiteSim on Ubuntu 12.04
-------------------------------------------

Start a terminal (keyboard: <ALT>+<CTRL>+<t>) and type:

mkdir 00PythonSoftware
cd 00PythonSoftware
sudo apt-get install git
git clone https://bitbucket.org/ufechner/FreeKiteSim.git
cd FreeKiteSim

The installation scripts, mentioned in the following sections are outdated.
Use the manual installation as explained in INSTALL_01.txt and INSTALL_02.txt 
instead.

Ubuntu, 64 bit:
---------------

sudo ./install_64.sh

Ubuntu, 32 bit:
---------------

sudo ./install_32.sh

The installation needs about 10 minutes on a fast PC. Press the <ENTER> key
whenever there is a question asked.

If you now type:

./kitesim.sh

two windows should pop up, the log-file player and the
3D viewer.

To learn more look at the manual at:
https://bitbucket.org/ufechner/freekitesim/src/master/doc/FreeKiteSim.rst
