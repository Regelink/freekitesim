""" List the content of a log-file. """
import pandas as pd

log_file = "log_8700W_8ms.df" # 8700W average mechanical cylce power, 8 m/s wind at 6 m height

df = pd.read_pickle(log_file)
print df

