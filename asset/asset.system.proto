package asset.system;

// The entries in the human readable log files are prefixed with the string representation of the MessageType,
// followed by ": ".
// The entries in the binary log files are prefixed with a byte with the number of the MessageType.
// Binary log fil entries have the following format:
// <0x59><message-size><message-type><message>
// UDP and ZeroMQ messages have the following format:
// <message-type><message>
// message-size: four bytes, the message size includes the message-type byte
// message-type: one byte
// message: encoded in the binary google protocal buffer format, size: message-size minus one
enum MessageType {
  mtProtoMsg        = 1; // meta message, that can contain all the other messages
  mtLogMsg          = 2; // default log messages, recorded on the AutoPilot laptop
  mtBrainyLog       = 3; // log messages, recorded in the control pod
  mtWinchLog        = 4; // log messages, recorded in the winch
  mtEventMsg        = 5; // event message
  mtGroundState     = 6; // messages, sent from the SensorDaemon (GPS position and windspeed)
  mtKiteCtrlState   = 7; // not used
  mtPodState        = 8; // messages from the KiteCtrlDaemon to KiteProxy and published by KiteProxy
  mtXSenseStream    = 9; // not used
  mtPodCtrl        = 10; // messages, sent from the KiteProxy to the KiteCtrlDaemon
  mtMiniWinchState = 11; // messages, published from the C# winch control laptop
  mtMiniWinchCtrl  = 12; // messages, sent from the WinchCtrlDaemon to the C# winch laptop
  mtPilotCtrlState = 13; // messages, sent from KiteProxy to PodControl
  mtEstimatedSystemState = 14; // messages published by the system state estimator
  mtDesiredTrajectory    = 15; // message published by flight path planner
  mtBearing              = 16; // message published by flight path controller
  mtAutopilotLog         = 17; // message published by flight path controller for logging
  mtStatistics           = 18;
  mtCourseControllerSettings = 19; // message published by central control
  mtWinchState           = 20; // messages published by SensorTask, later by the main task of the winch controller
  mtParticleSystem       = 21; // messages sent from KiteSim to to KiteViewer
}


// EG reference frame.
message Velocity_EG {           // velocity vector of the kite in the "EG" reference frame
  optional float v_east  = 1;   // v_east  [m/s]
  optional float v_north = 2;   // v_north [m/s]
  optional float v_up    = 3;   // v_up    [m/s]
}

message PositionEG {
   optional float pos_east   = 1; // position of the kite in east direction relative to the groundstation  [m]
   optional float pos_north  = 2; // position of the kite in north direction relative to the groundstation [m]
   optional float height     = 3; // height of the kite relative to the groundstation                      [m]
}
// the state of the particle system (tether, kite, later perhaps also the winch)
// only the information needed for 3D visualization
message ParticleSystem {
  required double time          =  1; // relative simulation time in seconds
  required double counter       =  2;
  repeated PositionEG positions =  3; // list of particle positions in EG reference frame
  optional double v_reel_out    =  4; // reel-out velocity                [m/s]
  optional double l_tether      =  5; // tether length                    [m/s]
  optional double force         =  6; // tether force                     [N]
  optional double v_wind_ground =  7; // ground wind speed                [m/s]
  optional double height        =  8; // height of the kite               [m]
  optional double v_wind_height =  9; // wind speed at height of the kite [m/s]
  optional double v_app_x       = 10; // apparent wind speed at the kite,  x direction (east)  [m/s]
  optional double v_app_y       = 11; // wind speed at height of the kite, y direction (north) [m/s]
  optional double v_app_z       = 12; // wind speed at height of the kite, z direction (up)    [m/s]
  optional double rel_steering  = 13; // relative steering        (-1.0 .. 1.0)
  optional double rel_depower   = 14; // relative depower         ( 0.0 .. 1.0)
  optional double azimuth       = 15; // azimuth
  optional double elevation     = 16; // elevation
  repeated Velocity_EG velocities = 17; // list of particle velocities in EG reference frame
  repeated PositionEG pos_predicted = 18; // list of predicted particle positions in EG reference frame
}

