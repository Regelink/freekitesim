# Installation script for the FreeKiteSim dependencies for Ubuntu 12.04, 64 bits

# install Python IDE and libraries
sudo apt-get install python-pip python-dev build-essential
sudo pip install -U pip
sudo pip install -U distribute
sudo apt-get install spyder
sudo apt-get install python-dev libfreetype6-dev
sudo apt-get install libpng12.dev
sudo pip install -U spyder
sudo pip install -U numpy
sudo pip install -U matplotlib
sudo pip install -U pyzmq
sudo pip install -U ipython
sudo pip install -U pandas
sudo pip install -U pyqtgraph
# create 00Software directory
cd ~
mkdir 00Software
cd ~/00Software
# install the dependencies of pygame
sudo apt-get install mercurial python-dev python-numpy ffmpeg libsdl1.2-dev libv4l-dev \
    libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev \
    libsdl1.2-dev  libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev
cd /usr/include/linux
sudo ln -s ../libv4l1-videodev.h videodev.h
cd ~/00Software
# install pygame from source
wget https://dl.dropboxusercontent.com/u/9075004/pygame-1.9.1release.tar.gz
tar xzf pygame-1.9.1release.tar.gz
cd pygame-1.9.1release
python setup.py build
sudo python setup.py install
cd ~/00Software
# install computer grapics kit
sudo apt-get install freeglut3 freeglut3-dev
sudo apt-get install python-opengl
wget https://dl.dropboxusercontent.com/u/9075004/cgkit-2.0.0-patched-32.tar.gz
tar xzf cgkit-2.0.0-patched-32.tar.gz
sudo apt-get install libboost-all-dev
sudo apt-get install lib3ds-dev
cd cgkit-2.0.0
sudo python setup.py install
cd ~/00PythonSoftware/FreeKiteSim/00_results
unxz -kf *.xz
cd ..



