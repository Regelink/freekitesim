# -*- coding: utf-8 -*-
""" Demo script that shows how to do linear algebra with cgkit types, 
compared with numpy. Using cgkit gives a big performance improvement
for 3d and 4d vectors if compared to numpy. """
from cgkit.cgtypes import vec3, mat3
from math import sqrt
import numpy as np

def linalg_cgkit():
    print "Linear algebra with cgkit:"
    v1 = vec3(1,2,3)
    v2 = vec3(4,5,6)
   
    print v1, v2

    # vector norm
    print v1.length() 
    
    # dot product (scalar product)
    print v1 * v2
    
    # cross product
    print v1.cross(v2)    
    
    # identity matrix
    print mat3(1.0),'\n'
    
    m1 = mat3(1, 2,   4,
              5, 7,   8,
              9, 11, 13)
              
    # matrix multiplication              
    print m1 * m1  
    
    # inverse of a matrix
    print m1.inverse()
    
def linalg_numpy():
    print "Linear algebra with numpy:"
    v1 = np.array((1,2,3))
    v2 = np.array((4,5,6))
    
    print v1,v2
    
    # vector norm
    print np.linalg.norm(v1)     
    
    # scalar product
    print v1.dot(v2)    
    
    # cross product
    print np.cross(v1, v2)    
    
    # identity matrix
    print np.eye(3),'\n'
    
    m1 = np.array(((1, 2, 4),
                   (5, 7, 8),
                   (9, 11, 13)))
                   
    # matrix multiplication
    print m1.dot(m1)
    
    # inverse of a matrix
    print np.linalg.inv(m1)
    
def rot2d(a, b):
    """ 
    Calculate the rotation of vector a so that it matches vector b.
    a, b must be of type cgkit.cgtypes.vec3. 
    """
    x = (a.cross(b)).normalize() 
    v = (a * b)/ a.length() / b.length()    
    A = mat3(0,   -x[2], x[1], 
             x[2], 0,   -x[0], 
            -x[1], x[0],   0)
    return mat3(1.0) + sqrt(1 - v*v) * A + v * A * A
    
def rot3d(ax, ay, az, bx, by, bz):
    """
    Calculate the rotation of reference frame (ax, ay, az) so that it matches the reference frame 
    (bx, by, bz).
    All parameters must be of type cgkit.cgtypes.vec3. Both refrence frames must be orthogonal,
    all vectors must already be normalized.
    Source: http://en.wikipedia.org/wiki/User:Snietfeld/TRIAD_Algorithm
    """
    R_ai = mat3(ax, az, ay)
    R_bi = mat3(bx, bz, by)
    return R_bi * R_ai.transpose()  
 
    
if __name__ == "__main__":
    #linalg_cgkit()    
    #print
    #linalg_numpy()
    ax, ay, az = vec3(1,0,0), vec3( 0,1,0), vec3(0,0,1)
    bx, by, bz = vec3(0,1,0), vec3(-1,0,0), vec3(0,0,1)
    vec = vec3(1,0,0)
    print vec
    print
    print rot3d(ax, ay, az, bx, by, bz)
    print rot3d(ax, ay, az, bx, by, bz) * vec
    
    #print
    #print rot2d(vec3(2,1,0), vec3(3,1,-1))
