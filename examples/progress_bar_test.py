import sys
from PyQt4 import QtGui, QtCore

# Class for demonstrating Progress Dialog
class ProgDialog(QtGui.QMainWindow):
	def __init__(self, app):
		QtGui.QMainWindow.__init__(self)
		self.pushButton = QtGui.QPushButton(self)
		self.pushButton.setText("Do Something")
		self.setCentralWidget(self.pushButton)
		self.connect(self.pushButton, QtCore.SIGNAL("clicked()"), self.doSomething)
		# Create progress dialog
		self.progDialog = QtGui.QProgressDialog("label text", "cancel button", 0, 100, self)
		self.progDialog.setModal(True)
		self.progDialog.setWindowTitle("title of progress dialog")
		self.progDialog.setMinimumDuration(1000)
		# Create thread object and connect its signals to methods on this object
		self.ponderous = PonderousTask()
		self.connect(self.ponderous, QtCore.SIGNAL("update(int)"), self.informOfUpdate)
		self.connect(self.ponderous, QtCore.SIGNAL("finished()"), self.informOfFinished)
		self.connect(self.progDialog, QtCore.SIGNAL("canceled()"), self.informOfCancelled)
		self.connect(self, QtCore.SIGNAL("cancelThread()"), self.ponderous.stopTask)

	# Method called on button click
	def doSomething(self):
		self.progDialog.reset()
		self.ponderous.goNow()
		print "done invoking thread"
	# Method called asynchronously by other thread when progress should be updated
	def informOfUpdate(self, inProgress):
		print "informed of update: ", inProgress
		if not self.progDialog.wasCanceled():
			self.progDialog.setValue(inProgress)
	# Method called asynchronously by other thread when it has finished
	def informOfFinished(self):
		print "informed of ponderous having finished!"
		self.progDialog.setValue(100)
		if self.progDialog.wasCanceled():
			print "Thread was cancelled, didn't complete properly"
		else:
			print "Success!"
	# Method called asynchronously by other thread when it has been cancelled
	def informOfCancelled(self):
		print "informed of ponderous having been cancelled!"
		self.emit(QtCore.SIGNAL("cancelThread()"))
		self.progDialog.cancel()

# Separate worker thread for time-consuming task
class PonderousTask(QtCore.QThread):
	def __init__(self, parent = None):
		QtCore.QThread.__init__(self, parent)
	# Call this to launch the thread
	def goNow(self, inNumLoops = 12000000):
		print "go now"
		self.numLoops = inNumLoops
		self.start()
	# This run method is called by Qt as a result of calling start()
	def run(self):
		print "Starting the ponderous task"
		self.stopping = False
		for t in range(10):
			l = 0
			for j in range(self.numLoops):
				l = l + 1
			# Check whether we've been cancelled or not
			if self.stopping:
				break
			# Don't interact directly with main thread, just emit signal
			self.emit(QtCore.SIGNAL("update(int)"), (t+1) * 10)
		print "Finished the ponderous task"
	def stopTask(self):
		self.stopping = True

# Run the whole thing by showing the ProgDialog object
app = QtGui.QApplication(sys.argv)
window = ProgDialog(app)
window.show()
sys.exit(app.exec_())

